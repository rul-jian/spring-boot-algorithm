package com.bibao.boot.algorithm.sort;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.algorithm.sort.BubbleSort;
import com.bibao.boot.algorithm.sort.HeapSort;
import com.bibao.boot.algorithm.sort.MergeSort;
import com.bibao.boot.algorithm.sort.QuickSort;
import com.bibao.boot.config.AspectJConfig;
import com.bibao.boot.springbootalgorithm.SpringBootAlgorithmApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AspectJConfig.class, SpringBootAlgorithmApplication.class})
public class SortTest {
	private static List<Integer> targetList;
	
	@Value("${sort.size}")
	private int size;
	
	@Autowired
	private BubbleSort bubbleSort;
	
	@Autowired
	private MergeSort mergeSort;
	
	@Autowired
	private HeapSort heapSort;
	
	@Autowired
	private QuickSort quickSort;
	
	@Before
	public void setUp() throws Exception {
		targetList = new ArrayList<>();
		for (int i=1; i<=size; i++) targetList.add(i);
		Collections.shuffle(targetList);
	}

	@Test
	public void testBubbleSort() {
		List<Integer> target = new ArrayList<>(targetList);
		bubbleSort.sort(target, (a, b) -> a - b);
		for (int i=0; i<size; i++) {
			assertEquals(i+1, target.get(i).intValue());
		}
	}

	@Test
	public void testMergeSort() {
		List<Integer> target = new ArrayList<>(targetList);
		mergeSort.sort(target, (a, b) -> a - b);
		for (int i=0; i<size; i++) {
			assertEquals(i+1, target.get(i).intValue());
		}
	}
	
	@Test
	public void testHeapSort() {
		List<Integer> target = new ArrayList<>(targetList);
		heapSort.sort(target, (a, b) -> a - b);
		for (int i=0; i<size; i++) {
			assertEquals(i+1, target.get(i).intValue());
		}
	}
	
	@Test
	public void testQuickSort() {
		List<Integer> target = new ArrayList<>(targetList);
		quickSort.sort(target, (a, b) -> a - b);
		for (int i=0; i<size; i++) {
			assertEquals(i+1, target.get(i).intValue());
		}
	}
}
