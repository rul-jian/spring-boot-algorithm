package com.bibao.boot.algorithm.prime;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.config.AspectJConfig;
import com.bibao.boot.springbootalgorithm.SpringBootAlgorithmApplication;
import com.bibao.boot.util.FileUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AspectJConfig.class, SpringBootAlgorithmApplication.class})
public class PrimeUtilTest {
	@Autowired
	private PrimeUtil primeUtil;
	
	@Autowired
	private FileUtil fileUtil;
	
	@Value("${prime.size}")
	private int size;
	
	@Test
	public void testGetPrimes() {
		List<Integer> primeList = primeUtil.getPrimes();
		fileUtil.writePrimes(primeList, "primelist1.txt");
		assertEquals(size, primeList.size());
	}

	@Test
	public void testGetPrimesEfficient() {
		List<Integer> primeList = primeUtil.getPrimesEfficient();
		fileUtil.writePrimes(primeList, "primelist2.txt");
		assertEquals(size, primeList.size());
	}
	
	@Test
	public void testGetPrimesCache() {
		List<Integer> primeList = primeUtil.getPrimesCache();
		fileUtil.writePrimes(primeList, "primelist3.txt");
		assertEquals(size, primeList.size());
	}
}
