package com.bibao.boot.algorithm.puzzle;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.algorithm.puzzle.TrianglePuzzle;
import com.bibao.boot.algorithm.puzzle.TriangleResult;
import com.bibao.boot.springbootalgorithm.SpringBootAlgorithmApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootAlgorithmApplication.class})
public class TrianglePuzzleTest {
	@Autowired
	private TrianglePuzzle puzzle;
	
	@Test
	public void testSolve() {
		int[] array = {5, 9, 4, 3, 1, 7, 2, 4, 8, 5};
		TriangleResult result = puzzle.solve(array);
		assertEquals(24, result.getSum());
		List<Integer> path = result.getPath();
		assertEquals(4, path.size());
		// Path: 0 -> 2 -> 5 -> 8
		assertEquals(0, path.get(0).intValue());
		assertEquals(2, path.get(1).intValue());
		assertEquals(5, path.get(2).intValue());
		assertEquals(8, path.get(3).intValue());
	}

}
