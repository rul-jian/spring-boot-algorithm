package com.bibao.boot.algorithm.puzzle;

import java.util.ArrayList;
import java.util.List;

public class TriangleResult {
	private List<Integer> path;
	private int sum;
	
	public TriangleResult() {
		path = new ArrayList<>();
	}
	
	public List<Integer> getPath() {
		return path;
	}
	public void setPath(List<Integer> path) {
		this.path = path;
	}
	public int getSum() {
		return sum;
	}
	public void setSum(int sum) {
		this.sum = sum;
	}
}
