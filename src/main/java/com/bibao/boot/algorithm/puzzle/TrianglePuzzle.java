package com.bibao.boot.algorithm.puzzle;

import java.util.Collections;

import org.springframework.stereotype.Component;

@Component
public class TrianglePuzzle {
	
	private class Node {
		int index;
		int value;
		int sumValue;
		int parentIndex;
		int level;
		
		Node(int index, int value, int level) {
			this.index = index;
			this.value = value;
			this.level = level;
			this.sumValue = Integer.MIN_VALUE;
			parentIndex = -1;
		}
		
		int leftChild() {
			return index + level;
		}
		
		int rightChild() {
			return index + level + 1;
		}
		
		
	}
	
	public TriangleResult solve(int[] array) {
		TriangleResult result = new TriangleResult();
		int n = array.length;
		// Convert the input array to an array of nodes
		Node[] nodes = new Node[n];
		int index = 0;
		int level = 1;
		while (index<n) {
			for (int i=0; i<level; i++) {
				nodes[index] = new Node(index, array[index], level);
				index++;
			}
			level++;
		}
		
		nodes[0].sumValue = nodes[0].value;
		// Go through the nodes and update sumValue for each node
		for (int i=0; i<n; i++) {
			Node currentNode = nodes[i];
			int leftChildIndex = currentNode.leftChild();
			int rightChildIndex = currentNode.rightChild();
			if (leftChildIndex<n) {
				int currentSum = currentNode.sumValue + nodes[leftChildIndex].value;
				if (currentSum>nodes[leftChildIndex].sumValue) {
					nodes[leftChildIndex].sumValue = currentSum;
					nodes[leftChildIndex].parentIndex = currentNode.index;
				}
			}
			if (rightChildIndex<n) {
				int currentSum = currentNode.sumValue + nodes[rightChildIndex].value;
				if (currentSum>nodes[rightChildIndex].sumValue) {
					nodes[rightChildIndex].sumValue = currentSum;
					nodes[rightChildIndex].parentIndex = currentNode.index;
				}
			}
		}
		// Find the node with max sumValue
		Node targetNode = nodes[0];
		for (Node node: nodes) {
			if (node.sumValue > targetNode.sumValue) {
				targetNode = node;
			}
		}
		
		// Fill the result 
		result.setSum(targetNode.sumValue);
		Node currentNode = targetNode;
		result.getPath().add(currentNode.index);
		while (currentNode.parentIndex>=0) {
			currentNode = nodes[currentNode.parentIndex];
			result.getPath().add(currentNode.index);
		}
		Collections.reverse(result.getPath());
		return result;
	}
}
