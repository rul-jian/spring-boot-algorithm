package com.bibao.boot.algorithm.sort;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class MergeSort implements GenericSort {

	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c) {
		sort(list, c, true);
	}

	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c, boolean ascending) {
		if (list==null || list.size()<=1) return;
		int n = list.size();
		int ascendingFlag = ascending? 1 : -1;
		List<T> tempList = new ArrayList<>(list);
		splitAndMerge(tempList, 0, n, list, c, ascendingFlag);
	}

	private <T> void splitAndMerge(List<T> tempList, int startIndex, int endIndex, List<T> list, 
			Comparator<? super T> c, int ascendingFlag) {
		if (endIndex - startIndex < 2) return;
		int middle = (startIndex + endIndex) / 2;
		splitAndMerge(list, startIndex, middle, tempList, c, ascendingFlag);
		splitAndMerge(list, middle, endIndex, tempList, c, ascendingFlag);
		merge(tempList, startIndex, middle, endIndex, list, c, ascendingFlag);
	}
	
	private <T> void merge(List<T> tempList, int startIndex, int middleIndex, int endIndex, List<T> list, 
			Comparator<? super T> c, int ascendingFlag) {
		int left = startIndex;
		int right = middleIndex;
		for (int i=startIndex; i<endIndex; i++) {
			if (left<middleIndex && (right>=endIndex || c.compare(tempList.get(left), tempList.get(right)) * ascendingFlag <= 0)) {
				list.set(i, tempList.get(left));
				left++;
			} else {
				list.set(i, tempList.get(right));
				right++;
			}
		}
	}
	
}
