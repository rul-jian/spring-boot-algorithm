package com.bibao.boot.algorithm.sort;

import java.util.Comparator;
import java.util.List;

public interface GenericSort {
	public <T> void sort(List<T> list, Comparator<? super T> c);
	public <T> void sort(List<T> list, Comparator<? super T> c, boolean ascending);
}
