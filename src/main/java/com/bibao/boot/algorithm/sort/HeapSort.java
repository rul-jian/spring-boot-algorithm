package com.bibao.boot.algorithm.sort;

import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class HeapSort implements GenericSort {

	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c) {
		sort(list, c, true);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c, boolean ascending) {
		if (list==null || list.size()<=1) return;
		Heap<T> heap = (Heap<T>) new Heap<>(c, ascending);
		list.forEach(v -> heap.add(v));
		for (int i=0; i<list.size(); i++) {
			list.set(i, heap.getMin());
		}
	}

}
