package com.bibao.boot.algorithm.sort;

import java.util.*;

public class Heap<T> {
	private List<T> list;
	private Comparator<T> c;
	private int ascendingFlag = 1;
	
	private Heap() {
		list = new ArrayList<>();
	}
	
	public Heap(Comparator<T> c) {
		this();
		this.c = c;
	}
	
	public Heap(Comparator<T> c, boolean ascending) {
		this(c);
		ascendingFlag = ascending? 1: -1;
	}
	
	public void add(T value) {
		list.add(value);
		int currentIndex = list.size() - 1;
		while (currentIndex>0) {
			int parentIndex = getParent(currentIndex);
			T v1 = list.get(parentIndex);
			T v2 = list.get(currentIndex);
			if (c.compare(v1, v2) * ascendingFlag > 0) {
				Collections.swap(list, parentIndex, currentIndex);
				currentIndex = parentIndex;
			} else {
				break;
			}
		}
	}
	
	public T getMin() {
		T first = list.get(0);
		Collections.swap(list, 0, list.size()-1);
		list.remove(list.size()-1);
		heapify(0);
		return first;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		list.forEach(v -> sb.append(v.toString() + " "));
		return sb.toString().trim();
	}
	
	private void heapify(int currentIndex) {
		int n = list.size();
		if (currentIndex>=n) return;
		T currentValue = list.get(currentIndex);
		int leftIndex = 2 * currentIndex + 1;
		int rightIndex = 2 * currentIndex + 2;
		int smallestIndex = currentIndex;
		if (leftIndex<n && c.compare(list.get(leftIndex), currentValue) * ascendingFlag < 0) {
			smallestIndex = leftIndex;
		}
		if (rightIndex<n && c.compare(list.get(rightIndex), list.get(smallestIndex)) * ascendingFlag < 0) {
			smallestIndex = rightIndex;
		}
		if (smallestIndex != currentIndex) {
			Collections.swap(list, currentIndex, smallestIndex);
			heapify(smallestIndex);
		}
	}
	
	private int getParent(int index) {
		if (index==0) return -1;
		return (index - 1) / 2;
	}
	
}
