package com.bibao.boot.algorithm.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class QuickSort implements GenericSort {

	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c) {
		sort(list, c, true);
	}

	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c, boolean ascending) {
		if (list==null || list.size()<=1) return;
		int n = list.size();
		int acendingFlag = ascending? 1 : -1;
		quickSort(list, 0, n-1, c, acendingFlag);
	}

	private <T> void quickSort(List<T> list, int low, int high, Comparator<? super T> c, int ascendingFlag) {
		if (low<high) {
			int p = partition(list, low, high, c, ascendingFlag);
			quickSort(list, low, p-1, c, ascendingFlag);
			quickSort(list, p+1, high, c, ascendingFlag);
		}
	}
	
	private <T> int partition(List<T> list, int low, int high, Comparator<? super T> c, int ascendingFlag) {
		T pivot = list.get(high);
		int current = low - 1;
		for (int i = low; i<high; i++) {
			if (c.compare(list.get(i), pivot) * ascendingFlag < 0) {
				current++;
				Collections.swap(list, current, i);
			}
		}
		Collections.swap(list, current + 1, high);
		return current + 1;
	}
	
}
