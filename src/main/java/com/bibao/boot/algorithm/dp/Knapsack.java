package com.bibao.boot.algorithm.dp;

import org.springframework.stereotype.Component;

@Component
public class Knapsack {
	
	public int maxValue(int[] values, int[] weights, int capacity) {
		int n = values.length;
		int[][] array = new int[n+1][capacity+1];
		for (int i=0; i<=n; i++) {
			for (int j=0; j<=capacity; j++) {
				if (i==0 || j==0) {
					array[i][j] = 0;
					continue;
				}
				if (weights[i-1]<=j) {
					array[i][j] = Math.max(values[i-1] + array[i-1][j - weights[i-1]], array[i-1][j]);
				} else {
					array[i][j] = array[i-1][j];
				}
			}
		}
		return array[n][capacity];
	}
}
