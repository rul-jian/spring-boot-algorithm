package com.bibao.boot.advice;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class RuntimeAdvice {
	
	@Around("execution(* com.bibao.boot.algorithm.prime.PrimeUtil.*(..))")
	public Object runtimeForPrimeUtil(ProceedingJoinPoint pjp) throws Throwable {
		Date startTime = new Date();
		Object result = pjp.proceed(pjp.getArgs());
		Date endTime = new Date();
		long gap = endTime.getTime() - startTime.getTime();
		String methodName = ((MethodSignature)pjp.getSignature()).getMethod().getName();
		System.out.println("Running time for " + methodName + ": " + gap);
		return result;
	}
	
	@Around("execution(* com.bibao.boot.algorithm.sort.*.sort(..))")
	public Object runtimeForSort(ProceedingJoinPoint pjp) throws Throwable {
		Date startTime = new Date();
		Object result = pjp.proceed(pjp.getArgs());
		Date endTime = new Date();
		long gap = endTime.getTime() - startTime.getTime();
		String className = pjp.getTarget().getClass().getName();
		System.out.println("Running time for " + className + ": " + gap);
		return result;
	}
}
